/**
 * Created by f3arnil on 09.04.15.
 */
var fs = require('fs');

var filename = __filename;

fs.stat(filename, function (err, data) {
    if (err){
        console.error(err);
    } else
    {
        console.log(data.isFile());
        if (data.isFile()){
            console.log(data);
            readMyFile(filename, function(err,dt){
                if (err){
                    console.error(err);
                } else {
                    console.log(dt);
                }
            });
        }
    }

})

function readMyFile(filename,callback){
    fs.readFile(filename, {encoding:'utf-8'}, function (err,data) {
        if (err){
            if (err.code === 'ENOENT'){
                //console.error(err.message);
                callback(err.message);
            } else {
                //console.error(err);
                callback(err);
            }

        } else {
            console.log(data.toString());
            callback(null,data.toString());
        }

    })
}

fs.writeFile('testFile.tmp', 'data', function(err){
    if (err) throw err;
    console.log('File created!');
    fs.rename('testFile.tmp', 'newTestFile.tmp', function(err){
        if (err) throw err;
        console.log('File renamed!');
        fs.unlink('newTestFile.tmp', function(err){
            if (err) throw err;
            console.log('File unlinked!');
        })
    })
})
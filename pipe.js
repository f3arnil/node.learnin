/**
 * Created by f3arnil on 09.04.15.
 */
var http = require('http');
var fs = require('fs');

new http.Server(function(req,res){

    if (req.url === '/debug.log'){
        var file = new fs.ReadStream('debug.log');
        sendFile(file, res);
    }
}).listen(3000);

function sendFile(file,res){

    file.pipe(res);

    file.on('error', function(err){
        res.statusCode = 500;
        res.end('Server error');
        console.log(err);
    });

    file.on('open', function(){
        console.log('open');
    });

    res.on('close', function(){
        file.destroy();
    });

    file.on('close', function(){
        console.log('close');
    });
}
/**
 * Created by f3arnil on 08.04.15.
 */
var http = require('http');
var fs = require('fs');

var server = new http.createServer(function(req,res){

    if (req.url === '/'){
        fs.readFile('index.html', function(err,data){
            if (err){
                console.error(err);
                res.statusCode = 500;
                res.end('На сервере произошла ошибка');
                return;
            } else {
                res.end(data);
            }
        });
    } else {
        res.statusCode = 404;
        res.end('Страница не найдена');
    }
}).listen(3000);



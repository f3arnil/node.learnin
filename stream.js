/**
 * Created by f3arnil on 09.04.15.
 */
var fs = require('fs');

var stream = new fs.ReadStream(__filename);

stream.on('readable', function(){
    var data = stream.read();
    if (data !== null)
    {
        console.log(data.toString());
    }
});

stream.on('end', function(){
    console.log('THE END');
});

stream.on('error', function(err){
    if (err.code === 'ENOENT'){
        console.log('Файл не найден!');
    } else {
        console.log(err);
    }
});
/**
 * Created by f3arnil on 08.04.15.
 */
var EventEmitter = require('events').EventEmitter;

var server = new EventEmitter;

server.on('request', function(request){
    request.approved = true;
});

server.on('request', function(request){
    console.log(request);
});

server.on('error', function(err){
    // Делаем что то
})

//console.log("Обработчики на событии request:\n" + server.listeners('request'));
//console.log("Количество обработчиков на событии request : " + EventEmitter.listenerCount(server, 'request'));

server.emit('request',{from:"Клиент"});

server.emit('request',{from:"Ещё Клиент"});

server.emit('error', new Error('Ошибка на стороне сервера'));


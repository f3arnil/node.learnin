/**
 * Created by f3arnil on 08.04.15.
 */
var http = require('http');
//var debug = require('debug')('server');
var log = require('./log')(module);

var server = new http.createServer();

server.on('request', require('./request'));

server.listen(1337, '127.0.0.1');
log.info('Server is running!');

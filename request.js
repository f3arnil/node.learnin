/**
 * Created by f3arnil on 08.04.15.
 */
var url = require('url');
var log = require('./log')(module);

module.exports = function(req,res){
    var urlParsed = url.parse(req.url, true); // true разберёт строку query в обьект
    log.info('Method = ' + req.method);
    log.info('PathName = ' + urlParsed.pathname);

    if (req.method === 'GET' && urlParsed.pathname ==='/echo' && urlParsed.query.message){
        var message = urlParsed.query.message;
        log.debug('ECHO: ' + message);
        res.end(message);
    }
    else {
        log.error('Unknown URL');
    }

    res.statusCode = 404;
    res.end('Page not found!');

}
